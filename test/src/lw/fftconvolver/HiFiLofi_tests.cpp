// ==================================================================================
// Copyright (c) 2017 HiFi-LoFi
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ==================================================================================

#include <lw/fftconvolver/FFTConvolver.hpp>
#include <lw/fftconvolver/TwoStageFFTConvolver.hpp>
#include <lw/fftconvolver/utilities.hpp>
#include <lw/mss.hpp>
#include <catch2/catch.hpp>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iostream>


template<typename T>
void SimpleConvolve(const T *input, size_t inLen, const T *ir, size_t irLen, T *output)
{
    if (irLen > inLen)
    {
        SimpleConvolve(ir, irLen, input, inLen, output);
        return;
    }

    std::fill_n(output, inLen + irLen -1, 0);

    for (size_t n = 0; n < irLen; ++n)
    {
        for (size_t m = 0; m <= n; ++m)
        {
            output[n] += ir[m] * input[n - m];
        }
    }

    for (size_t n = irLen; n < inLen; ++n)
    {
        for (size_t m = 0; m < irLen; ++m)
        {
            output[n] += ir[m] * input[n - m];
        }
    }

    for (size_t n = inLen; n < inLen + irLen - 1; ++n)
    {
        for (size_t m = n - inLen + 1; m < irLen; ++m)
        {
            output[n] += ir[m] * input[n - m];
        }
    }
}

template<typename T, typename InitFunc>
static bool test_convolver(size_t inputSize, size_t irSize, size_t blockSizeMin, size_t blockSizeMax, InitFunc &&init, bool refCheck)
{
    MSS_BEGIN(bool);


    using sample_t = typename T::sample_t;

    // Prepare input and IR
    std::vector<sample_t> in(inputSize);
    for (size_t i = 0; i < inputSize; ++i)
    {
        in[i] = 0.1f * static_cast<sample_t>(i + 1);
    }

    std::vector<sample_t> ir(irSize);
    for (size_t i = 0; i < irSize; ++i)
    {
        ir[i] = 0.1f * static_cast<sample_t>(i + 1);
    }

    // Simple convolver
    std::vector<sample_t> outSimple(in.size() + ir.size() - 1, sample_t(0.0));
    if (refCheck)
    {
        SimpleConvolve(&in[0], in.size(), &ir[0], ir.size(), &outSimple[0]);
    }

    // FFT convolver
    std::vector<sample_t> out(in.size() + ir.size() - 1, sample_t(0.0));
    {
        T convolver;
        MSS(init(convolver, ir.data(), ir.size()));

        std::vector<sample_t> inBuf(blockSizeMax);
        size_t processedOut = 0;
        size_t processedIn = 0;
        while (processedOut < out.size())
        {
            const size_t blockSize = blockSizeMin + (static_cast<size_t>(rand()) % (1 + (blockSizeMax - blockSizeMin)));

            const size_t remainingOut = out.size() - processedOut;
            const size_t remainingIn = in.size() - processedIn;

            const size_t processingOut = std::min(remainingOut, blockSize);
            const size_t processingIn = std::min(remainingIn, blockSize);

            std::fill(inBuf.begin(), inBuf.end(), 0);
            if (processingIn > 0)
            {
                std::copy_n(in.data() + processedIn, processingIn, inBuf.data());
            }

            MSS(convolver.process(&inBuf[0], &out[processedOut], processingOut));

            processedOut += processingOut;
            processedIn += processingIn;
        }
    }

    if (refCheck)
    {
        size_t diffSamples = 0;
        const double absTolerance = 0.001 * static_cast<double>(ir.size());
        const double relTolerance = 0.0001 * ::log(static_cast<double>(ir.size()));
        for (size_t i = 0; i < outSimple.size(); ++i)
        {
            const double a = static_cast<double>(out[i]);
            const double b = static_cast<double>(outSimple[i]);
            if (::fabs(a) > 1.0 && ::fabs(b) > 1.0)
            {
                const double absError = ::fabs(a - b);
                const double relError = absError / b;
                if (relError > relTolerance && absError > absTolerance)
                {
                    ++diffSamples;
                }
            }
        }

        MSS(diffSamples == 0, std::cout << "Out of tolerance bounds for " << diffSamples << " number of samples" << std::endl);
    }

    MSS_END();
}

#define TEST_CORRECTNESS
//#define TEST_PERFORMANCE

#define TEST_FFTCONVOLVER
#define TEST_TWOSTAGEFFTCONVOLVER

TEST_CASE("HiFi-LoFi original tests", "[ut][fftconvolver]")
{
    SECTION("float")
    {
        using sample_t = float;

        SECTION("One stage convolver")
        {
            using T = lw::fftconvolver::FFTConvolver<sample_t>;
            auto make_init = [](unsigned int block_size) {
                return [=](T &t, const sample_t *ir, unsigned int length) {
                    return t.init(block_size, ir, length);
                };
            };

#if defined(TEST_CORRECTNESS) && defined(TEST_FFTCONVOLVER)
            REQUIRE(test_convolver<T>(1, 1, 1, 1, make_init(1), true));
            REQUIRE(test_convolver<T>(2, 2, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(3, 3, 3, 3, make_init(3), true));

            REQUIRE(test_convolver<T>(3, 2, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(4, 2, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(4, 3, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(9, 4, 3, 3, make_init(2), true));
            REQUIRE(test_convolver<T>(171, 7, 5, 5, make_init(5), true));
            REQUIRE(test_convolver<T>(1979, 17, 7, 7, make_init(5), true));
            REQUIRE(test_convolver<T>(100, 10, 3, 5, make_init(5), true));
            REQUIRE(test_convolver<T>(123, 45, 12, 34, make_init(34), true));

            REQUIRE(test_convolver<T>(2, 3, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(2, 4, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(3, 4, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(4, 9, 3, 3, make_init(3), true));
            REQUIRE(test_convolver<T>(7, 171, 5, 5, make_init(5), true));
            REQUIRE(test_convolver<T>(17, 1979, 7, 7, make_init(7), true));
            REQUIRE(test_convolver<T>(10, 100, 3, 5, make_init(5), true));
            REQUIRE(test_convolver<T>(45, 123, 12, 34, make_init(34), true));

            REQUIRE(test_convolver<T>(100000, 1234, 100, 128, make_init(128), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 256, make_init(256), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 512, make_init(512), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 1024, make_init(1024), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 2048, make_init(2048), true));

            REQUIRE(test_convolver<T>(100000, 4321, 100, 128, make_init(128), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 256, make_init(256), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 512, make_init(512), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 1024, make_init(1024), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 2048, make_init(2048), true));
#endif

#if defined(TEST_PERFORMANCE) && defined(TEST_FFTCONVOLVER)
            REQUIRE(test_convolver(3 * 60 * 44100, 20 * 44100, 50, 100, make_init(1024), false));
#endif
        }

        SECTION("Two stage convolver")
        {
            using T = lw::fftconvolver::TwoStageFFTConvolver<sample_t>;
            auto make_init = [](unsigned int head_block_size, unsigned int tail_block_size) {
                return [=](T &t, const sample_t *ir, unsigned int length) {
                    return t.init(head_block_size, tail_block_size, ir, length);
                };
            };

#if defined(TEST_CORRECTNESS) && defined(TEST_TWOSTAGEFFTCONVOLVER)
            REQUIRE(test_convolver<T>(1, 1, 1, 1, make_init(1, 1), true));
            REQUIRE(test_convolver<T>(2, 2, 2, 2, make_init(2, 2), true));
            REQUIRE(test_convolver<T>(3, 3, 3, 3, make_init(3, 3), true));

            REQUIRE(test_convolver<T>(3, 2, 2, 2, make_init(2, 4), true));
            REQUIRE(test_convolver<T>(4, 2, 2, 2, make_init(2, 4), true));
            REQUIRE(test_convolver<T>(4, 3, 2, 2, make_init(2, 4), true));
            REQUIRE(test_convolver<T>(9, 4, 3, 3, make_init(2, 4), true));
            REQUIRE(test_convolver<T>(171, 7, 5, 5, make_init(5, 10), true));
            REQUIRE(test_convolver<T>(1979, 17, 7, 7, make_init(5, 10), true));
            REQUIRE(test_convolver<T>(100, 10, 3, 5, make_init(5, 10), true));
            REQUIRE(test_convolver<T>(123, 45, 12, 34, make_init(34, 68), true));

            REQUIRE(test_convolver<T>(2, 3, 2, 2, make_init(1, 2), true));
            REQUIRE(test_convolver<T>(2, 4, 2, 2, make_init(1, 2), true));
            REQUIRE(test_convolver<T>(3, 4, 2, 2, make_init(1, 2), true));
            REQUIRE(test_convolver<T>(4, 9, 3, 3, make_init(2, 4), true));
            REQUIRE(test_convolver<T>(7, 171, 5, 5, make_init(2, 16), true));
            REQUIRE(test_convolver<T>(17, 1979, 7, 7, make_init(4, 16), true));
            REQUIRE(test_convolver<T>(10, 100, 3, 5, make_init(1, 4), true));
            REQUIRE(test_convolver<T>(45, 123, 12, 34, make_init(4, 32), true));

            REQUIRE(test_convolver<T>(100000, 1234, 100, 128, make_init(128, 4096), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 256, make_init(256, 4096), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 512, make_init(512, 4096), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 1024, make_init(1024, 4096), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 2048, make_init(2048, 4096), true));

            REQUIRE(test_convolver<T>(100000, 4321, 100, 128, make_init(128, 4096), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 256, make_init(256, 4096), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 512, make_init(512, 4096), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 1024, make_init(1024, 4096), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 2048, make_init(2048, 4096), true));
#endif

#if defined(TEST_PERFORMANCE) && defined(TEST_TWOSTAGEFFTCONVOLVER)
            REQUIRE(test_convolver<T>(3 * 60 * 44100, 20 * 44100, 50, 100, make_init(100, 2 * 8192), false));
#endif
        }
    }
    
    SECTION("double")
    {
        using sample_t = double;

        SECTION("One stage convolver")
        {
            using T = lw::fftconvolver::FFTConvolver<sample_t>;
            auto make_init = [](unsigned int block_size) {
                return [=](T &t, const sample_t *ir, unsigned int length) {
                    return t.init(block_size, ir, length);
                };
            };

#if defined(TEST_CORRECTNESS) && defined(TEST_FFTCONVOLVER)
            REQUIRE(test_convolver<T>(1, 1, 1, 1, make_init(1), true));
            REQUIRE(test_convolver<T>(2, 2, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(3, 3, 3, 3, make_init(3), true));

            REQUIRE(test_convolver<T>(3, 2, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(4, 2, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(4, 3, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(9, 4, 3, 3, make_init(2), true));
            REQUIRE(test_convolver<T>(171, 7, 5, 5, make_init(5), true));
            REQUIRE(test_convolver<T>(1979, 17, 7, 7, make_init(5), true));
            REQUIRE(test_convolver<T>(100, 10, 3, 5, make_init(5), true));
            REQUIRE(test_convolver<T>(123, 45, 12, 34, make_init(34), true));

            REQUIRE(test_convolver<T>(2, 3, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(2, 4, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(3, 4, 2, 2, make_init(2), true));
            REQUIRE(test_convolver<T>(4, 9, 3, 3, make_init(3), true));
            REQUIRE(test_convolver<T>(7, 171, 5, 5, make_init(5), true));
            REQUIRE(test_convolver<T>(17, 1979, 7, 7, make_init(7), true));
            REQUIRE(test_convolver<T>(10, 100, 3, 5, make_init(5), true));
            REQUIRE(test_convolver<T>(45, 123, 12, 34, make_init(34), true));

            REQUIRE(test_convolver<T>(100000, 1234, 100, 128, make_init(128), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 256, make_init(256), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 512, make_init(512), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 1024, make_init(1024), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 2048, make_init(2048), true));

            REQUIRE(test_convolver<T>(100000, 4321, 100, 128, make_init(128), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 256, make_init(256), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 512, make_init(512), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 1024, make_init(1024), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 2048, make_init(2048), true));
#endif

#if defined(TEST_PERFORMANCE) && defined(TEST_FFTCONVOLVER)
            REQUIRE(test_convolver<T>(3 * 60 * 44100, 20 * 44100, 50, 100, make_init(1024), false));
#endif
        }

        SECTION("Two stage convolver")
        {
            using T = lw::fftconvolver::TwoStageFFTConvolver<sample_t>;
            auto make_init = [](unsigned int head_block_size, unsigned int tail_block_size) {
                return [=](T &t, const sample_t *ir, unsigned int length) {
                    return t.init(head_block_size, tail_block_size, ir, length);
                };
            };

#if defined(TEST_CORRECTNESS) && defined(TEST_TWOSTAGEFFTCONVOLVER)
            REQUIRE(test_convolver<T>(1, 1, 1, 1, make_init(1, 1), true));
            REQUIRE(test_convolver<T>(2, 2, 2, 2, make_init(2, 2), true));
            REQUIRE(test_convolver<T>(3, 3, 3, 3, make_init(3, 3), true));

            REQUIRE(test_convolver<T>(3, 2, 2, 2, make_init(2, 4), true));
            REQUIRE(test_convolver<T>(4, 2, 2, 2, make_init(2, 4), true));
            REQUIRE(test_convolver<T>(4, 3, 2, 2, make_init(2, 4), true));
            REQUIRE(test_convolver<T>(9, 4, 3, 3, make_init(2, 4), true));
            REQUIRE(test_convolver<T>(171, 7, 5, 5, make_init(5, 10), true));
            REQUIRE(test_convolver<T>(1979, 17, 7, 7, make_init(5, 10), true));
            REQUIRE(test_convolver<T>(100, 10, 3, 5, make_init(5, 10), true));
            REQUIRE(test_convolver<T>(123, 45, 12, 34, make_init(34, 68), true));

            REQUIRE(test_convolver<T>(2, 3, 2, 2, make_init(1, 2), true));
            REQUIRE(test_convolver<T>(2, 4, 2, 2, make_init(1, 2), true));
            REQUIRE(test_convolver<T>(3, 4, 2, 2, make_init(1, 2), true));
            REQUIRE(test_convolver<T>(4, 9, 3, 3, make_init(2, 4), true));
            REQUIRE(test_convolver<T>(7, 171, 5, 5, make_init(2, 16), true));
            REQUIRE(test_convolver<T>(17, 1979, 7, 7, make_init(4, 16), true));
            REQUIRE(test_convolver<T>(10, 100, 3, 5, make_init(1, 4), true));
            REQUIRE(test_convolver<T>(45, 123, 12, 34, make_init(4, 32), true));

            REQUIRE(test_convolver<T>(100000, 1234, 100, 128, make_init(128, 4096), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 256, make_init(256, 4096), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 512, make_init(512, 4096), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 1024, make_init(1024, 4096), true));
            REQUIRE(test_convolver<T>(100000, 1234, 100, 2048, make_init(2048, 4096), true));

            REQUIRE(test_convolver<T>(100000, 4321, 100, 128, make_init(128, 4096), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 256, make_init(256, 4096), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 512, make_init(512, 4096), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 1024, make_init(1024, 4096), true));
            REQUIRE(test_convolver<T>(100000, 4321, 100, 2048, make_init(2048, 4096), true));
#endif

#if defined(TEST_PERFORMANCE) && defined(TEST_TWOSTAGEFFTCONVOLVER)
            REQUIRE(test_convolver<T>(3 * 60 * 44100, 20 * 44100, 50, 100, make_init(100, 2 * 8192), false));
#endif
        }
    }
}
