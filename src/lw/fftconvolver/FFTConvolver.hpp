// ==================================================================================
// Copyright (c) 2017 HiFi-LoFi
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ==================================================================================

#ifndef _FFTCONVOLVER_FFTCONVOLVER_HPP
#define _FFTCONVOLVER_FFTCONVOLVER_HPP

#include <lw/fftconvolver/SplitComplex.hpp>
#include <lw/ooura/FFT.hpp>

#include <vector>
#include <memory>

namespace lw { namespace fftconvolver {

    template <typename T>
    class FFTConvolver
    {
    public:
        using sample_t = T;
        using value_type = T;
        using FFT = lw::ooura::FFT<sample_t>;
        FFTConvolver();

        bool init(size_t blockSize, const sample_t *ir, size_t irLen);
        bool process(const sample_t *input, sample_t *output, size_t len);

        void reset_audio_state();
        void clear();

    private:
        void advance_(std::size_t todo);

        using SplitComplexPtr = std::shared_ptr<SplitComplex<sample_t>>;

        std::size_t block_size_;
        std::size_t num_segments_;
        std::vector<SplitComplexPtr> input_segments_;
        std::vector<SplitComplexPtr> filter_segments_;
        Buffer<sample_t> fft_buffer_;
        FFT fft_;
        SplitComplex<sample_t> pre_multiplied_;
        SplitComplex<sample_t> conv_;
        Buffer<sample_t> add_overlap_;
        std::size_t current_segment_idx_;
        Buffer<sample_t> input_buffer_;
        std::size_t input_buffer_idx_;

        FFTConvolver(const FFTConvolver &) = delete;
        FFTConvolver &operator=(const FFTConvolver &) = delete;
    };

}}

#endif // Header guard
