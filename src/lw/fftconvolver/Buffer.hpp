// ==================================================================================
// Copyright (c) 2017 HiFi-LoFi
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ==================================================================================

#ifndef HEADER_lw_fftconvolver_Buffer_hpp_ALREADY_INCLUDED
#define HEADER_lw_fftconvolver_Buffer_hpp_ALREADY_INCLUDED

#include <algorithm>
#include <cassert>
#include <utility>

namespace lw { namespace fftconvolver {

    namespace utility {

        void *allocate(std::size_t num_element, std::size_t element_size);
        void deallocate(void *ptr);

    }

    template<typename T>
    class Buffer
    {
    public:
        using value_type = T;
        using sample_t = T;
        explicit Buffer(std::size_t initialSize = 0)
            : data_(nullptr)
            , size_(0)
        {
            resize(initialSize);
        }

        virtual ~Buffer()
        {
            clear();
        }

        void clear()
        {
            deallocate(data_);
            data_ = nullptr;
            size_ = 0;
        }

        void resize(std::size_t size)
        {
            if (size_ != size)
            {
                clear();

                if (size > 0)
                {
                    assert(!data_ && size_ == 0);
                    data_ = allocate(size);
                    size_ = size;
                }
            }
            set_zero();
        }

        std::size_t size() const
        {
            return size_;
        }

        void set_zero()
        {
            std::fill_n(data_, size_, 0);
        }

        void copy_from(const Buffer<T> &other)
        {
            assert(size_ == other.size_);
            if (this != &other)
            {
                std::copy_n(other.data_, size_, data_);
            }
        }

        T &operator[](std::size_t index)
        {
            assert(data_ && index < size_);
            return data_[index];
        }

        const T &operator[](std::size_t index) const
        {
            assert(data_ && index < size_);
            return data_[index];
        }

        operator bool() const
        {
            return (data_ != 0 && size_ > 0);
        }

        T *data()
        {
            return data_;
        }

        const T *data() const
        {
            return data_;
        }

        friend void swap(Buffer<T> &a, Buffer<T> &b)
        {
            using std::swap;

            swap(a.data_, b.data_);
            swap(a.size_, b.size_);
        }

    private:
        static T *allocate(std::size_t size)
        {
            return static_cast<T *>(utility::allocate(size, sizeof(T)));
        }

        void deallocate(T *ptr)
        {
            utility::deallocate(ptr);
        }

        T *data_;
        std::size_t size_;

        // Prevent uncontrolled usage
        Buffer(const Buffer &) = delete;
        Buffer &operator=(const Buffer &) = delete;
    };

}}

#endif
