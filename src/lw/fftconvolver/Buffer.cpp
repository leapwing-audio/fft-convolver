#include <lw/fftconvolver/Buffer.hpp>
#include <lw/fftconvolver/SSE.hpp>

namespace lw { namespace fftconvolver {

    namespace utility {

        void *allocate(std::size_t num_elements, std::size_t element_size)
        {
#if defined(FFTCONVOLVER_USE_SSE)
            return _mm_malloc(num_elements * element_size, 16);
#else
            return malloc(num_elements*element_size);
#endif
        }

        void deallocate(void *ptr)
        {
#if defined(FFTCONVOLVER_USE_SSE)
            _mm_free(ptr);
#else
            free(ptr);
#endif
        }
    }
}}

