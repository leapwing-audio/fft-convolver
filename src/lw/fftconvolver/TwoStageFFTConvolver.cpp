// ==================================================================================
// Copyright (c) 2017 HiFi-LoFi
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ==================================================================================

#include <lw/fftconvolver/TwoStageFFTConvolver.hpp>
#include <lw/mss.hpp>

#include <algorithm>
#include <cmath>

namespace lw { namespace fftconvolver {

    template<typename T>
    TwoStageFFTConvolver<T>::TwoStageFFTConvolver()
        : head_block_size_(0)
        , tail_block_size_(0)
        , head_convolver_()
        , tail_convolver_0_()
        , tail_output_0_()
        , tail_precalculated_0_(0)
        , tail_convertor_()
        , tail_output_()
        , tail_precalculated_(0)
        , tail_input_()
        , tail_input_fill_(0)
        , precalculated_pos_(0)
        , background_processing_input_()
    {
    }

    template<typename T>
    void TwoStageFFTConvolver<T>::reset()
    {
        head_block_size_ = 0;
        tail_block_size_ = 0;
        head_convolver_.clear();
        tail_convolver_0_.clear();
        tail_output_0_.clear();
        tail_precalculated_0_.clear();
        tail_convertor_.clear();
        tail_output_.clear();
        tail_precalculated_.clear();
        tail_input_.clear();
        tail_input_fill_ = 0;
        tail_input_fill_ = 0;
        precalculated_pos_ = 0;
        background_processing_input_.clear();
    }

    template<typename T>
    bool TwoStageFFTConvolver<T>::init(size_t headBlockSize, size_t tailBlockSize, const sample_t *ir, size_t irLen)
    {
        MSS_BEGIN(bool);

        reset();

        MSS(headBlockSize > 0);
        MSS(tailBlockSize > 0);

        MSS(headBlockSize <= tailBlockSize);

        MSS(irLen > 0);

        head_block_size_ = NextPowerOf2(headBlockSize);
        tail_block_size_ = NextPowerOf2(tailBlockSize);

        const size_t headIrLen = std::min(irLen, tail_block_size_);
        MSS(head_convolver_.init(head_block_size_, ir, headIrLen));

        if (irLen > tail_block_size_)
        {
            const size_t conv1IrLen = std::min(irLen - tail_block_size_, tail_block_size_);
            MSS(tail_convolver_0_.init(head_block_size_, ir + tail_block_size_, conv1IrLen));
            tail_output_0_.resize(tail_block_size_);
            tail_precalculated_0_.resize(tail_block_size_);
        }

        if (irLen > 2 * tail_block_size_)
        {
            const size_t tailIrLen = irLen - (2 * tail_block_size_);
            MSS(tail_convertor_.init(tail_block_size_, ir + (2 * tail_block_size_), tailIrLen));
            tail_output_.resize(tail_block_size_);
            tail_precalculated_.resize(tail_block_size_);
            background_processing_input_.resize(tail_block_size_);
        }

        if (tail_precalculated_0_.size() > 0 || tail_precalculated_.size() > 0)
        {
            tail_input_.resize(tail_block_size_);
        }
        tail_input_fill_ = 0;
        precalculated_pos_ = 0;

        MSS_END();
    }

    template<typename T>
    bool TwoStageFFTConvolver<T>::process(const sample_t *input, sample_t *output, size_t len)
    {
        MSS_BEGIN(bool);

        // Head
        MSS(head_convolver_.process(input, output, len));

        // Tail
        if (tail_input_.size() > 0)
        {
            size_t processed = 0;
            while (processed < len)
            {
                const size_t remaining = len - processed;
                const size_t processing = std::min(remaining, head_block_size_ - (tail_input_fill_ % head_block_size_));
                assert(tail_input_fill_ + processing <= tail_block_size_);

                // Sum head and tail
                const size_t sumBegin = processed;
                const size_t sumEnd = processed + processing;
                {
                    // Sum: 1st tail block
                    if (tail_precalculated_0_.size() > 0)
                    {
                        size_t precalculatedPos = precalculated_pos_;
                        for (size_t i = sumBegin; i < sumEnd; ++i)
                        {
                            output[i] += tail_precalculated_0_[precalculatedPos];
                            ++precalculatedPos;
                        }
                    }

                    // Sum: 2nd-Nth tail block
                    if (tail_precalculated_.size() > 0)
                    {
                        size_t precalculatedPos = precalculated_pos_;
                        for (size_t i = sumBegin; i < sumEnd; ++i)
                        {
                            output[i] += tail_precalculated_[precalculatedPos];
                            ++precalculatedPos;
                        }
                    }

                    precalculated_pos_ += processing;
                }

                // Fill input buffer for tail convolution
                std::copy_n(input + processed, processing, tail_input_.data() + tail_input_fill_);
                tail_input_fill_ += processing;
                assert(tail_input_fill_ <= tail_block_size_);

                // Convolution: 1st tail block
                if (tail_precalculated_0_.size() > 0 && tail_input_fill_ % head_block_size_ == 0)
                {
                    assert(tail_input_fill_ >= head_block_size_);
                    const size_t blockOffset = tail_input_fill_ - head_block_size_;
                    MSS(tail_convolver_0_.process(tail_input_.data() + blockOffset, tail_output_0_.data() + blockOffset, head_block_size_));
                    if (tail_input_fill_ == tail_block_size_)
                    {
                        swap(tail_precalculated_0_, tail_output_0_);
                    }
                }

                // Convolution: 2nd-Nth tail block (might be done in some background thread)
                if (tail_precalculated_.size() > 0 && tail_input_fill_ == tail_block_size_ && background_processing_input_.size() == tail_block_size_ && tail_output_.size() == tail_block_size_)
                {
                    swap(tail_precalculated_, tail_output_);
                    background_processing_input_.copy_from(tail_input_);
                    MSS(tail_convertor_.process(background_processing_input_.data(), tail_output_.data(), tail_block_size_));
                }

                if (tail_input_fill_ == tail_block_size_)
                {
                    tail_input_fill_ = 0;
                    precalculated_pos_ = 0;
                }

                processed += processing;
            }
        }

        MSS_END();
    }

    template class TwoStageFFTConvolver<double>;
    template class TwoStageFFTConvolver<float>;

}}
