// ==================================================================================
// Copyright (c) 2017 HiFi-LoFi
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ==================================================================================
//
#ifndef HEADER_fftconvolver_SplitComplex_hpp_ALREADY_INCLUDED
#define HEADER_fftconvolver_SplitComplex_hpp_ALREADY_INCLUDED

#include <lw/fftconvolver/Buffer.hpp>
#include <lw/Range.hpp>

namespace lw { namespace fftconvolver {

    template<typename T>
    class SplitComplex
    {
    public:
        explicit SplitComplex(std::size_t initialSize = 0)
            : size_(0), re_(), im_()
        {
            resize(initialSize);
        }

        ~SplitComplex()
        {
            clear();
        }

        void clear()
        {
            re_.clear();
            im_.clear();
            size_ = 0;
        }

        void resize(std::size_t newSize)
        {
            re_.resize(newSize);
            im_.resize(newSize);
            size_ = newSize;
        }

        void set_zero()
        {
            re_.set_zero();
            im_.set_zero();
        }

        void copy_from(const SplitComplex &other)
        {
            re_.copy_from(other.re_);
            im_.copy_from(other.im_);
        }

        T *re()
        {
            return re_.data();
        }

        const T *re() const
        {
            return re_.data();
        }

        T *im()
        {
            return im_.data();
        }

        const T *im() const
        {
            return im_.data();
        }

        std::size_t size() const
        {
            return size_;
        }

    private:
        std::size_t size_;
        Buffer<T> re_;
        Buffer<T> im_;

        SplitComplex(const SplitComplex &) = delete;
        SplitComplex &operator=(const SplitComplex &) = delete;
    };
    
    template <typename T>
    lw::Range<T> real_range(SplitComplex<T> & ctr)
    {
        return range(ctr.re(), ctr.size());
    }

    template <typename T>
    lw::Range<T> imag_range(SplitComplex<T> & ctr)
    {
        return range(ctr.im(), ctr.size());
    }
    
    template <typename T>
    lw::ConstRange<T> real_crange(const SplitComplex<T> & ctr)
    {
        return range(ctr.re(), ctr.size());
    }

    template <typename T>
    lw::ConstRange<T> imag_crange(const SplitComplex<T> & ctr)
    {
        return range(ctr.im(), ctr.size());
    }

}}

#endif
