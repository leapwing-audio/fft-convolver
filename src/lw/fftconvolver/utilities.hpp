// ==================================================================================
// Copyright (c) 2017 HiFi-LoFi
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ==================================================================================

#ifndef HEADER_lw_fftconvolver_utilities_hpp_ALREADY_INCLUDED
#define HEADER_lw_fftconvolver_utilities_hpp_ALREADY_INCLUDED

#include <lw/fftconvolver/Buffer.hpp>
#include <lw/fftconvolver/SplitComplex.hpp>

#include <algorithm>
#include <cassert>
#include <cstddef>

#if defined(__GNUC__)
#define FFTCONVOLVER_RESTRICT __restrict__
#else
#define FFTCONVOLVER_RESTRICT
#endif

namespace lw { namespace fftconvolver {

    template<typename T>
    T NextPowerOf2(const T &val)
    {
        T nextPowerOf2 = 1;
        while (nextPowerOf2 < val)
        {
            nextPowerOf2 *= 2;
        }
        return nextPowerOf2;
    }

    template<typename T>
    void copy_and_pad(Buffer<T> &dst, const T *src, size_t src_size)
    {
        assert(dst.size() >= src_size);
        std::copy_n(src, src_size, dst.data());
        std::fill_n(dst.data() + src_size, dst.size() - src_size, 0);
    }

    template<typename T>
    void sum(T *FFTCONVOLVER_RESTRICT result, const T *FFTCONVOLVER_RESTRICT a, const T *FFTCONVOLVER_RESTRICT b, size_t len)
    {
        const std::size_t end4 = 4 * (len / 4);
        for (std::size_t i = 0; i < end4; i += 4)
        {
            result[i + 0] = a[i + 0] + b[i + 0];
            result[i + 1] = a[i + 1] + b[i + 1];
            result[i + 2] = a[i + 2] + b[i + 2];
            result[i + 3] = a[i + 3] + b[i + 3];
        }
        for (std::size_t i = end4; i < len; ++i)
        {
            result[i] = a[i] + b[i];
        }
    }

    template<typename T>
    void multiply_accumulate(SplitComplex<T> &result, const SplitComplex<T> &a, const SplitComplex<T> &b);

}}

#endif // Header guard
