// ==================================================================================
// Copyright (c) 2017 HiFi-LoFi
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ==================================================================================

#include <lw/fftconvolver/FFTConvolver.hpp>
#include <lw/fftconvolver/utilities.hpp>
#include <lw/mss.hpp>

#include <cassert>
#include <cmath>

namespace lw { namespace fftconvolver {

    template<typename T>
    FFTConvolver<T>::FFTConvolver()
        : block_size_(0)
        , num_segments_(0)
        , input_segments_()
        , filter_segments_()
        , fft_buffer_()
        , fft_()
        , pre_multiplied_()
        , conv_()
        , add_overlap_()
        , current_segment_idx_(0)
        , input_buffer_()
        , input_buffer_idx_(0)
    {
    }

    template<typename T>
    void FFTConvolver<T>::clear()
    {
        block_size_ = 0;
        num_segments_ = 0;
        input_segments_.clear();
        filter_segments_.clear();
        fft_buffer_.clear();
        fft_.init(0, FFT::DivideInvByN);
        pre_multiplied_.clear();
        conv_.clear();
        add_overlap_.clear();
        current_segment_idx_ = 0;
        input_buffer_.clear();
        input_buffer_idx_ = 0;
    }

    template<typename T>
    void FFTConvolver<T>::reset_audio_state()
    {
        for (auto &as : input_segments_)
            as->set_zero();
        fft_buffer_.set_zero();
        pre_multiplied_.set_zero();
        conv_.set_zero();
        add_overlap_.set_zero();
        current_segment_idx_ = 0;
        input_buffer_.set_zero();
        input_buffer_idx_ = 0;
    }

    template<typename T>
    bool FFTConvolver<T>::init(size_t blockSize, const sample_t *ir, size_t irLen)
    {
        MSS_BEGIN(bool);

        clear();

        MSS(blockSize > 0);

        if (irLen == 0)
            MSS_RETURN_OK();

        // initialize the fft 
        unsigned int order = std::ceil(std::log2(blockSize));
        block_size_ = (1u << order);

        MSS(fft_.init(order+1, FFT::DivideInvByN));
        std::size_t segment_size = fft_.time_domain_size();
        std::size_t fft_complex_size = fft_.freq_domain_size();

        num_segments_ = static_cast<size_t>(std::ceil(static_cast<float>(irLen) / static_cast<float>(block_size_)));

        // FFT
        fft_buffer_.resize(segment_size);

        // Prepare segments
        for (size_t i = 0; i < num_segments_; ++i)
        {
            input_segments_.push_back(std::make_shared<SplitComplex<sample_t>>(fft_complex_size));
        }

        // Prepare IR
        for (size_t i = 0; i < num_segments_; ++i)
        {
            const std::size_t start_idx = i * block_size_;
            const std::size_t to_copy = std::min<std::size_t>(irLen - start_idx, block_size_);

            copy_and_pad(fft_buffer_, ir + start_idx, to_copy);

            auto segment = std::make_shared<SplitComplex<sample_t>>(fft_complex_size);
            fft_.fft(fft_buffer_.data(), segment->re(), segment->im());
            filter_segments_.push_back(segment);
        }

        // Prepare convolution buffers
        pre_multiplied_.resize(fft_complex_size);
        conv_.resize(fft_complex_size);
        add_overlap_.resize(block_size_);

        // Prepare input buffer
        input_buffer_.resize(block_size_);
        input_buffer_idx_ = 0;

        // Reset current position
        current_segment_idx_ = 0;

        MSS_END();
    }

    template<typename T>
    bool FFTConvolver<T>::process(const sample_t *input, sample_t *output, size_t len)
    {
        MSS_BEGIN(bool);

        if (num_segments_ == 0)
        {
            std::fill_n(output, len, 0);
            MSS_RETURN_OK();
        }

        size_t processed = 0;
        while (processed < len)
        {
            // process till end of current segment or end of data
            const size_t todo = std::min(len - processed, block_size_ - input_buffer_idx_);

            // add the input to the input buffer
            std::copy_n(input, todo, input_buffer_.data() + input_buffer_idx_);

            // copy to the fft buffer, padded with zeros
            copy_and_pad(fft_buffer_, input_buffer_.data(), block_size_);

            // Forward FFT
            SplitComplex<sample_t> &cur_segment_freq = *input_segments_[current_segment_idx_];
            MSS(fft_.fwd(crange(fft_buffer_), real_range(cur_segment_freq), imag_range(cur_segment_freq)));

            // start from the pre multiplied values
            conv_.copy_from(pre_multiplied_);

            // add the multiplication of our segment with the ir
            multiply_accumulate(conv_, cur_segment_freq, *filter_segments_[0]);

            // Backward FFT
            MSS(fft_.inv(real_crange(conv_), imag_crange(conv_), range(fft_buffer_)));

            // Add overlap
            sum(output, fft_buffer_.data() + input_buffer_idx_, add_overlap_.data() + input_buffer_idx_, todo);

            // update counters
            processed += todo;
            input += todo;
            output += todo;

            // advance in the input buffer
            advance_(todo);
        }

        MSS_END();
    }

    template<typename T>
    void FFTConvolver<T>::advance_(std::size_t todo)
    {
        input_buffer_idx_ += todo;

        // end of the input buffer ?
        if (input_buffer_idx_ == block_size_)
        {
            // clear the input buffer for next run
            input_buffer_.set_zero();
            input_buffer_idx_ = 0;

            // Save the overlap
            std::copy_n(fft_buffer_.data() + block_size_, block_size_, add_overlap_.data());

            // Update current segment
            current_segment_idx_ = (current_segment_idx_ > 0) ? (current_segment_idx_ - 1) : (num_segments_ - 1);

            // pre multiply the already completed blocks
            pre_multiplied_.set_zero();
            for (size_t i = 1; i < num_segments_; ++i)
            {
                const size_t indexIr = i;
                const size_t indexAudio = (current_segment_idx_ + i) % num_segments_;
                multiply_accumulate(pre_multiplied_, *filter_segments_[indexIr], *input_segments_[indexAudio]);
            }
        }
    }

    template class FFTConvolver<double>;
    template class FFTConvolver<float>;

}}
