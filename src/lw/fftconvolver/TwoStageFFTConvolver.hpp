// ==================================================================================
// Copyright (c) 2017 HiFi-LoFi
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ==================================================================================

#ifndef HEADER_lw_fftconvolver_TwoStageFFTConvolver_hpp_ALREADY_INCLUDED
#define HEADER_lw_fftconvolver_TwoStageFFTConvolver_hpp_ALREADY_INCLUDED

#include <lw/fftconvolver/FFTConvolver.hpp>
#include <lw/fftconvolver/utilities.hpp>

namespace lw { namespace fftconvolver {

    template<typename T>
    class TwoStageFFTConvolver
    {
    public:
        using sample_t = T;
        TwoStageFFTConvolver();

        bool init(size_t headBlockSize, size_t tailBlockSize, const sample_t *ir, std::size_t irLen);
        bool process(const sample_t *input, sample_t *output, std::size_t len);

        void reset();

    protected:
    private:
        std::size_t head_block_size_;
        std::size_t tail_block_size_;
        FFTConvolver<sample_t> head_convolver_;
        FFTConvolver<sample_t> tail_convolver_0_;
        Buffer<sample_t> tail_output_0_;
        Buffer<sample_t> tail_precalculated_0_;
        FFTConvolver<sample_t> tail_convertor_;
        Buffer<sample_t> tail_output_;
        Buffer<sample_t> tail_precalculated_;
        Buffer<sample_t> tail_input_;
        std::size_t tail_input_fill_;
        std::size_t precalculated_pos_;
        Buffer<sample_t> background_processing_input_;

        // Prevent uncontrolled usage
        TwoStageFFTConvolver(const TwoStageFFTConvolver &) = delete;
        TwoStageFFTConvolver &operator=(const TwoStageFFTConvolver &) = delete;
    };

}
}

#endif
