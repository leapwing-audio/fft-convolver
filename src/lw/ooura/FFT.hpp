// ==================================================================================
// Copyright (c) 2017 HiFi-LoFi
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ==================================================================================

#ifndef HEADER_lw_ooura_FFT_hpp_ALREADY_INCLUDED
#define HEADER_lw_ooura_FFT_hpp_ALREADY_INCLUDED

#include <lw/Range.hpp>
#include <utility>
#include <vector>
#include <complex>

namespace lw { namespace ooura {

    template <typename T>
    class FFT
    {
    public:
        using sample_t = T;
        using complex_t = std::complex<sample_t>;
    
        enum Mode
        {
            DivideBySqrtN,
            DivideInvByN,
            DivideFwdByN,
        };

        FFT();
        FFT(FFT &&) = default;
        FFT &operator=(FFT &&) = default;
       
        bool init(unsigned int order, Mode mode = DivideInvByN);
        bool initialize(unsigned int order, Mode mode = DivideInvByN) {return init(order, mode); }
        void fft(const sample_t *in, sample_t *out_re, sample_t *out_im) const;
        void fft(const sample_t *in, complex_t * cmplx) const;
        void ifft(sample_t *data, const sample_t *re, const sample_t *im) const;
        void ifft(sample_t *data, const complex_t * cmplx) const;

        bool fwd(const lw::ConstRange<sample_t> & src, const lw::Range<complex_t> & dst) const;
        bool inv(const lw::ConstRange<complex_t> & src, const lw::Range<sample_t> & dst) const;
        bool fwd(const lw::ConstRange<sample_t> & data, const lw::Range<sample_t> & re, const lw::Range<sample_t> & im) const;
        bool inv(const lw::ConstRange<sample_t> & re, const lw::ConstRange<sample_t> & im, const lw::Range<sample_t> & data) const;

        static constexpr std::size_t time_domain_size(std::size_t order) { return (1u << order); }
        static constexpr std::size_t freq_domain_size(std::size_t order) { return (1u << (order - 1)) + 1; }

        std::size_t order() const { return order_; }
        std::size_t time_domain_size() const { return size_; }
        std::size_t freq_domain_size() const { return 1 + size_/2; }

    private:
        FFT(const FFT &) = delete;
        FFT &operator=(const FFT &) = delete;

        unsigned int order_;
        std::size_t size_;
        sample_t fwd_scale_ = 0, inv_scale_ = 0;

        mutable struct
        {
            std::vector<int> ip;
            std::vector<double> w;
            std::vector<double> buffer;
        } wd;
    };

} }

#endif
